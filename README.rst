=======
Snippet
=======

A command-line interface for creating, inspecting, and editing 
Bitbucket Snippets.

-----
Using
-----

Install
=======

.. code-block:: shell

    pip install git+https://bitbucket.org/atlassian/snippet.git

First Time
==========

.. code-block:: shell

    snippet --help        # Just to make sure install is OK.
    snippet auth --set    # The CLI will store credentials in your home directory
                          # in plain-text.
    snippet list          # See the list of snippets in your account.

----------
Developing
----------

Python Virtual Environment Setup (for OS X)
===========================================

It's not virtual like a virtual machine. More like a specialized "container" for a Python version and libraries.

1. :code:`brew install python` This installs the latest version of Python 2.7 with a version of setuptools and pip. Unfortunately, those versions of setuptools and pip seem to be broken.
2. :code:`pip install --upgrade --no-use-wheel setuptools`
3. :code:`pip install --upgrade --no-use-wheel pip`
4. :code:`pip install virtualenvwrapper`

Project Setup
=============

1. Clone the repository and set it as the current working directory.
2. *(Optional, but good practice)* Create a `virtual environment <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_: :code:`mkvirtualenv snippet`. Once created, use :code:`workon snippet` to restore the virtual environment.
3. :code:`pip install -r requirements-dev.txt` Loads required libraries into the virtual environment.
4. :code:`paver test_all` Run all the unit tests (there aren't many) and analyze the source code.

To pull cutting edge changes from PyBitbucket:

.. code-block:: shell

    pip install git+https://bitbucket.org/atlassian/python-bitbucket.git --upgrade

----
TODO
----

* When skipping public and listed switches, :code:`modify` still sets public and listed attributes. Also, listed switch is no longer relevant.
* Watch: check status, add, and remove.
* Comments: :code:`POST` a new comment.
