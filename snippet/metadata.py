# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

package = 'snippet'
project = 'Snippet'
project_no_spaces = project.replace(' ', '')
version = '0.5.0'
description = ('A command-line interface for creating, inspecting, and '
               'editing Bitbucket Snippets.')
authors = ['Ian Buchanan']
authors_string = ', '.join(authors)
emails = ['ibuchanan@atlassian.com']
license = 'Apache'
copyright = '2015 ' + 'Atlassian'
url = 'https://bitbucket.org/atlassian/snippet'
