def get_first(iterable, default=None):
    if iterable:
        for item in iterable:
            return item
    return default


def is_private(public, private):
    if not public and not private:
        return False
    return ((not public) or private)
